//
//  Presupuesto_x_Producto_x_Puesto.swift
//  Mercatec_IOS
//
//  Created by Alumnos on 11/3/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit

class Presupuesto_x_Producto_x_Puesto: NSObject {
    var ID=0
    var PresupuestoID=0
    var Producto_x_PuestoID=0
    var Cantidad:Double=0.0
    var Precio:Double=0.0
    var Total:Double=0.0
    var PuestoAcronimo:String=""
    var NombreProducto:String=""
}
