//
//  LoginViewController.swift
//  Mercatec_IOS
//
//  Created by Alumnos on 11/3/18.
//  Copyright © 2018 Alumnos. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var txtUsuario: UITextField!
    
    @IBOutlet weak var txtContraseña: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //let usuario = UserDefaults.standard.string(forKey: "usuario")
        //self.txtUsuario.text = usuario
    }
    
    @IBAction func btnIniciarSesion(_ sender: Any) {
        
        let usuario = self.txtUsuario.text
        let contraseña = self.txtContraseña.text
        
        if ( usuario != "" && contraseña != ""){
            
            Alamofire.request("http://johnne-001-site1.itempurl.com/api/login?username=" + usuario! + "&password=" + contraseña!).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value as? NSNull{
                    
                    let alert = UIAlertController(title: "¡Error!", message: "Usuario o contraseña inválida", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let json = response.result.value
                    print("JSON: \(json)") // serialized json response
                     let userJson = JSON(json)
                     let id = userJson["ID"].intValue
                     let usuario = userJson["Username"].stringValue
                    
                    
                     //Stored in phone
                     //UserDefaults.standard.set(id, forKey: "usuarioID")
                     //UserDefaults.standard.set(usuario, forKey: "username")
                     
                     self.performSegue(withIdentifier: "cuentas", sender: self)
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }
        }
        else{
            let alert = UIAlertController(title: "¡Error!", message: "Por favor complete todos los campos", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnOlvidasteContraseña(_ sender: Any) {
        
    }
    
    @IBAction func btnNoTieneCuenta(_ sender: Any) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
